const connection = require('../database');

function generateTicketNumber() {
    return Math.floor(100000 + Math.random() * 900000);
}

exports.getSite = async (req, res) => {
    try {
        const { name, location, categoryOfEquipment, userMail, ownerMail } = req.query;

        let sql = 'SELECT * FROM site WHERE 1=1';
        let queryParams = [];

        if (name) {
            sql += ' AND name = ?';
            queryParams.push(name);
        }
        if (location) {
            sql += ' AND location = ?';
            queryParams.push(location);
        }
        if (categoryOfEquipment) {
            sql += ' AND categoryOfEquipment = ?';
            queryParams.push(categoryOfEquipment);
        }
        if (userMail) {
            sql += ' AND userMail = ?';
            queryParams.push(userMail);
        }
        if (ownerMail) {
            sql += ' AND ownerMail = ?';
            queryParams.push(ownerMail);
        }

        connection.query(sql, queryParams, function (err, result) {
            if (err) {
                console.error('Database error: ', err);
                res.status(500).json({ error: "Failed to get data" });
                return;
            }

            if (result && result.length > 0) {
                res.status(200).json(result);
            } else {
                res.status(404).json({ error: 'Site details not found' });
            }
        });
    } catch (error) {
        console.error('Unexpected error: ', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};

exports.createSite = async (req, res) => {
    try {
        const { name, categoryOfEquipment, location, description, userMail, ownerMail } = req.body;
        const ticketNumber = generateTicketNumber();

        const sql = `INSERT INTO site (ticketNumber, name, categoryOfEquipment, location, description, userMail, ownerMail) VALUES (?, ?, ?, ?, ?, ?, ?)`;

        connection.query(sql, [ticketNumber, name, categoryOfEquipment, location, description, userMail, ownerMail], (err, result) => {
            if (err) {
                console.error('Database error: ', err);
                res.status(500).json({ error: "Failed to create data" });
                return;
            }
            res.status(201).json({ message: "Site created successfully", ticketNumber });
        });
    } catch (error) {
        console.error('Unexpected error: ', error);
        res.status(500).send(error);
    }
};

exports.updateSite = async (req, res) => {
    try {
        const { ticketNumber } = req.params;
        const { location } = req.body;

        if (!ticketNumber || !location) {
            res.status(400).json({ error: "Ticket number and location are required" });
            return;
        }

        const sql = "UPDATE site SET location = ? WHERE ticketNumber = ?";

        connection.query(sql, [location, ticketNumber], function (err, result) {
            if (err) {
                console.error("Database error: ", err);
                res.status(500).json({ error: "Failed to update data" });
                return;
            }

            if (result.affectedRows === 0) {
                res.status(404).json({ error: "Site not found" });
                return;
            }

            res.status(200).json({ message: "Site updated successfully" });
        });
    } catch (error) {
        console.error("Unexpected error: ", error);
        res.status(500).send(error);
    }
};

exports.deleteSite = async (req, res) => {
    try {
        const { ticketNumber } = req.query;

        if (!ticketNumber) {
            res.status(400).json({ error: "Ticket number is required" });
            return;
        }

        const sql = "DELETE FROM site WHERE ticketNumber = ?";

        connection.query(sql, [ticketNumber], function (err, result) {
            if (err) {
                console.error('Database error: ', err);
                res.status(500).json({ error: "Failed to delete data" });
                return;
            }

            if (result.affectedRows === 0) {
                res.status(404).json({ error: "Site not found" });
                return;
            }

            res.status(200).json({ message: "Site deleted successfully" });
        });
    } catch (error) {
        console.error('Unexpected error: ', error);
        res.status(500).send(error);
    }
};
module.exports.generateTicketNumber = generateTicketNumber
