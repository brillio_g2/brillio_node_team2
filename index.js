const express = require("express");
const cors = require('cors');
const inventoryRouter = require("./inventoryService/routes/inventory");
const siteRouter = require("./inventoryService/routes/siteservicesjh");

const app = express();

/* Middleware */
app.use(express.json()); // to parse JSON bodies
app.use(express.urlencoded({ extended: true })); // to parse URL-encoded bodies

// CORS configuration
const corsOptions = {
    origin: true,
    methods: ["POST", "GET", "PUT"],
    credentials: true,
    maxAge: 3600
};
app.use(cors(corsOptions));

// Middleware to add request date
const requestDate = (req, res, next) => {
    req.requestDate = new Date().toLocaleString('en-US', { timeZone: 'Asia/Kolkata' });
    next();
};
app.use(requestDate);

// Routes
app.use('/inventory', inventoryRouter);
app.use('/site', siteRouter);

// Start the server
const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Listening on server at port ${PORT}`);
});

module.exports = app;
