const connection = require('../database')
const { v4:uuid4} = require('uuid')

function generateProductId(){
    const prefix ='PROD';
    const uniqueId = Math.floor(10000 + Math.random() * 10000);
    return  `${prefix}${uniqueId}`
}
exports.getInventory = async (req, res) => {
    try {
        let sql = 'SELECT ownerMail,location,categoryOfEquipment,availableQty FROM site_inventory_management.inventory';
        console.log(sql);
        connection.query(sql, function (err, result) {
            if (err) {
                console.error('Database error: ', err);
                res.status(500).json({ error: "Failed to get data" });
                return;
            }

            console.log(result);
            if (result && result.length > 0) {
                res.status(200).json(result);
            } else {
                res.status(404).json({ error: 'Inventory details not found' });
            }
        });
    } catch (error) {
        console.error('Unexpected error: ', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};


exports.createInventory = async (req, res) => {
    try {
        const items = req.body;

        if (!Array.isArray(items)) {
            return res.status(400).send('Request body should be an array of items');
        }

        let sql = "INSERT INTO site_inventory_management.inventory (productId, name, categoryOfEquipment, totalQty, availableQty, location, description, ownerMail) VALUES ?";
        const values = items.map(item => [
            generateProductId(),
            item.name,
            item.categoryOfEquipment,
            item.totalQty,
            item.totalQty, // Setting availableQty to totalQty
            item.location,
            item.description,
            item.ownerMail
        ]);

        connection.query(sql, [values], function (err, result) {
            if (err) {
                console.error('Database error: ', err);
                res.status(500).json({ error: "Failed to create data" });
                return;
            }
            console.log(result);
            res.status(201).json({ message: "Created successfully in inventory", result });
        });
    } catch (error) {
        console.error('Unexpected error: ', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};


exports.updateInventory = async (req, res) => {
    try {
        const productId  = req.params.productId;
        const { availableQty } = req.body;

        if (!productId || availableQty === undefined) {
            res.status(400).json({ error: "Product ID and available quantity are required" });
            return;
        }

        let sql = "UPDATE site_inventory_management.inventory SET availableQty = ? WHERE productId = ?";
        console.log("Executing SQL: ", sql);

        connection.query(sql, [availableQty, productId], function (err, result) {
            if (err) {
                console.error("Database error: ", err);
                res.status(500).json({ error: "Failed to update data" });
                return;
            }

            if (result.affectedRows === 0) {
                res.status(404).json({ error: "Product not found" });
                return;
            }

            console.log("Update result: ", result);
            res.status(200).json({ message: "Data updated successfully" });
        });
    } catch (error) {
        console.error("Unexpected error: ", error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
}


exports.deleteInventory = async (req, res) => {
    try {
        const productId  = req.params.productId;
        let sql = "DELETE FROM site_inventory_management.inventory WHERE productId = ?";

        connection.query(sql, [productId], function (err, result) {
            if (err) {
                console.error('Database error: ', err);
                res.status(500).json({ error: "Failed to delete data" });
                return;
            }

            if (result.affectedRows === 0) {
                res.status(404).json({ error: "Product not found" });
                return;
            }

            console.log(result);
            res.status(200).json({ message: "Deleted successfully" });
        });
    } catch (error) {
        console.error('Unexpected error: ', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};
