const express = require('express');
const router = express.Router();
const siteservices = require('../services/site');

router.get('/', siteservices.getSite);
router.post('/', siteservices.createSite);
router.put('/:ticketNumber', siteservices.updateSite);
router.delete('/', siteservices.deleteSite);

module.exports = router;
