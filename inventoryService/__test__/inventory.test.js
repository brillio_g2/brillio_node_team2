const { getInventory, createInventory, updateInventory, deleteInventory } = require('../services/inventory');
const connection = require('../database');

jest.mock('../database');

describe('Inventory Management', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('getInventory', () => {
        it('should return inventory details', async () => {
            const req = {};
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };
            const mockData = [{ ownerMail: 'test@example.com', location: 'A1', categoryOfEquipment: 'Tools', availableQty: 10 }];

            connection.query.mockImplementation((sql, callback) => {
                callback(null, mockData);
            });

            await getInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(expect.any(String), expect.any(Function));
            expect(res.status).toHaveBeenCalledWith(200);
            expect(res.json).toHaveBeenCalledWith(mockData);
        });

        it('should handle database errors', async () => {
            const req = {};
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation((sql, callback) => {
                callback(new Error('Database error'), null);
            });

            await getInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(expect.any(String), expect.any(Function));
            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({ error: 'Failed to get data' });
        });

        it('should handle empty inventory', async () => {
            const req = {};
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation((sql, callback) => {
                callback(null, []);
            });

            await getInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(expect.any(String), expect.any(Function));
            expect(res.status).toHaveBeenCalledWith(404);
            expect(res.json).toHaveBeenCalledWith({ error: 'Inventory details not found' });
        });

        it('should handle unexpected errors', async () => {
            const req = {};
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation(() => {
                throw new Error('Unexpected error');
            });

            await getInventory(req, res);

            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({ error: 'Internal Server Error' });
        });
    });

    describe('createInventory', () => {
        it('should create inventory items', async () => {
            const req = {
                body: [
                    {
                        name: 'Hammer',
                        categoryOfEquipment: 'Tools',
                        totalQty: 10,
                        location: 'A1',
                        description: 'A useful hammer',
                        ownerMail: 'test@example.com',
                    },
                ],
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation((sql, values, callback) => {
                callback(null, { affectedRows: 1 });
            });

            await createInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(
                expect.any(String),
                expect.any(Array),
                expect.any(Function)
            );
            expect(res.status).toHaveBeenCalledWith(201);
            expect(res.json).toHaveBeenCalledWith(expect.objectContaining({ message: 'Created successfully in inventory' }));
        });

        it('should handle invalid request body', async () => {
            const req = {
                body: {
                    name: 'Hammer',
                    categoryOfEquipment: 'Tools',
                    totalQty: 10,
                    location: 'A1',
                    description: 'A useful hammer',
                    ownerMail: 'test@example.com',
                },
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                send: jest.fn(),
            };

            await createInventory(req, res);

            expect(res.status).toHaveBeenCalledWith(400);
            expect(res.send).toHaveBeenCalledWith('Request body should be an array of items');
        });

        it('should handle database errors', async () => {
            const req = {
                body: [
                    {
                        name: 'Hammer',
                        categoryOfEquipment: 'Tools',
                        totalQty: 10,
                        location: 'A1',
                        description: 'A useful hammer',
                        ownerMail: 'test@example.com',
                    },
                ],
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation((sql, values, callback) => {
                callback(new Error('Database error'), null);
            });

            await createInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(
                expect.any(String),
                expect.any(Array),
                expect.any(Function)
            );
            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({ error: 'Failed to create data' });
        });

        it('should handle unexpected errors', async () => {
            const req = {
                body: [
                    {
                        name: 'Hammer',
                        categoryOfEquipment: 'Tools',
                        totalQty: 10,
                        location: 'A1',
                        description: 'A useful hammer',
                        ownerMail: 'test@example.com',
                    },
                ],
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            jest.spyOn(global.console, 'error').mockImplementation(() => {});

            connection.query.mockImplementation(() => {
                throw new Error('Unexpected error');
            });

            await createInventory(req, res);

            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({ error: 'Internal Server Error' });
        });
    });

    describe('updateInventory', () => {
        it('should update inventory item', async () => {
            const req = {
                params: { productId: 'PROD1234' },
                body: { availableQty: 5 },
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation((sql, values, callback) => {
                callback(null, { affectedRows: 1 });
            });

            await updateInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(
                expect.any(String),
                expect.any(Array),
                expect.any(Function)
            );
            expect(res.status).toHaveBeenCalledWith(200);
            expect(res.json).toHaveBeenCalledWith({ message: 'Data updated successfully' });
        });

        it('should handle invalid request parameters', async () => {
            const req = {
                params: {},
                body: { availableQty: 5 },
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            await updateInventory(req, res);

            expect(res.status).toHaveBeenCalledWith(400);
            expect(res.json).toHaveBeenCalledWith({ error: 'Product ID and available quantity are required' });
        });

        it('should handle database errors', async () => {
            const req = {
                params: { productId: 'PROD1234' },
                body: { availableQty: 5 },
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation((sql, values, callback) => {
                callback(new Error('Database error'), null);
            });

            await updateInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(
                expect.any(String),
                expect.any(Array),
                expect.any(Function)
            );
            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({ error: 'Failed to update data' });
        });

        it('should handle product not found', async () => {
            const req = {
                params: { productId: 'PROD1234' },
                body: { availableQty: 5 },
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation((sql, values, callback) => {
                callback(null, { affectedRows: 0 });
            });

            await updateInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(
                expect.any(String),
                expect.any(Array),
                expect.any(Function)
            );
            expect(res.status).toHaveBeenCalledWith(404);
            expect(res.json).toHaveBeenCalledWith({ error: 'Product not found' });
        });

        it('should handle unexpected errors', async () => {
            const req = {
                params: { productId: 'PROD1234' },
                body: { availableQty: 5 },
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            jest.spyOn(global.console, 'error').mockImplementation(() => {});

            connection.query.mockImplementation(() => {
                throw new Error('Unexpected error');
            });

            await updateInventory(req, res);

            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({ error: 'Internal Server Error' });
        });
    });

    describe('deleteInventory', () => {
        it('should delete inventory item', async () => {
            const req = {
                params: { productId: 'PROD1234' },
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation((sql, values, callback) => {
                callback(null, { affectedRows: 1 });
            });

            await deleteInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(
                expect.any(String),
                expect.any(Array),
                expect.any(Function)
            );
            expect(res.status).toHaveBeenCalledWith(200);
            expect(res.json).toHaveBeenCalledWith({ message: 'Deleted successfully' });
        });

        it('should handle database errors', async () => {
            const req = {
                params: { productId: 'PROD1234' },
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation((sql, values, callback) => {
                callback(new Error('Database error'), null);
            });

            await deleteInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(
                expect.any(String),
                expect.any(Array),
                expect.any(Function)
            );
            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({ error: 'Failed to delete data' });
        });

        it('should handle product not found', async () => {
            const req = {
                params: { productId: 'PROD1234' },
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            connection.query.mockImplementation((sql, values, callback) => {
                callback(null, { affectedRows: 0 });
            });

            await deleteInventory(req, res);

            expect(connection.query).toHaveBeenCalledWith(
                expect.any(String),
                expect.any(Array),
                expect.any(Function)
            );
            expect(res.status).toHaveBeenCalledWith(404);
            expect(res.json).toHaveBeenCalledWith({ error: 'Product not found' });
        });

        it('should handle unexpected errors', async () => {
            const req = {
                params: { productId: 'PROD1234' },
            };
            const res = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn(),
            };

            jest.spyOn(global.console, 'error').mockImplementation(() => {});

            connection.query.mockImplementation(() => {
                throw new Error('Unexpected error');
            });

            await deleteInventory(req, res);

            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({ error: 'Internal Server Error' });
        });
    });
});
