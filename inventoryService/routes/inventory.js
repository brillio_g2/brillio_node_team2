const express = require('express');
const inventoryService = require('../services/inventory');
const inventoryRouter = express.Router();

// Define routes and associate them with service methods
inventoryRouter.get('/get', inventoryService.getInventory);
inventoryRouter.post('/create', inventoryService.createInventory);
inventoryRouter.put('/update/:productId', inventoryService.updateInventory);
inventoryRouter.delete('/delete/:productId', inventoryService.deleteInventory);

module.exports = inventoryRouter;
