const request = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');
const siteController = require('./services/siteservice'); // Update the path accordingly

const app = express();
app.use(bodyParser.json());

// Mock database connection
jest.mock('./database', () => ({
    query: jest.fn()
}));

const connection = require('./database');

app.get('/site', siteController.getSite);
app.post('/site', siteController.createSite);
app.put('/site/:ticketNumber', siteController.updateSite);
app.delete('/site', siteController.deleteSite);

describe('Site Controller', () => {
    describe('GET /site', () => {
        it('should return site details', async () => {
            const mockData = [{ ticketNumber: 123456, name: 'Test Site' }];
            connection.query.mockImplementation((sql, params, callback) => {
                callback(null, mockData);
            });

            const response = await request(app).get('/site').query({ name: 'Test Site' });

            expect(response.status).toBe(200);
            expect(response.body).toEqual(mockData);
        });

        it('should return 404 if no site details found', async () => {
            connection.query.mockImplementation((sql, params, callback) => {
                callback(null, []);
            });

            const response = await request(app).get('/site').query({ name: 'Nonexistent Site' });

            expect(response.status).toBe(404);
            expect(response.body.error).toBe('Site details not found');
        });
    });

    describe('POST /site', () => {
        it('should create a new site', async () => {
            connection.query.mockImplementation((sql, params, callback) => {
                callback(null, { insertId: 1 });
            });

            const siteData = {
                name: 'New Site',
                categoryOfEquipment: 'Category',
                location: 'Location',
                description: 'Description',
                userMail: 'user@example.com',
                ownerMail: 'owner@example.com'
            };

            const response = await request(app).post('/site').send(siteData);

            expect(response.status).toBe(201);
            expect(response.body.message).toBe('Site created successfully');
        });

        it('should return 500 if creation fails', async () => {
            connection.query.mockImplementation((sql, params, callback) => {
                callback(new Error('Database error'), null);
            });

            const siteData = {
                name: 'New Site',
                categoryOfEquipment: 'Category',
                location: 'Location',
                description: 'Description',
                userMail: 'user@example.com',
                ownerMail: 'owner@example.com'
            };

            const response = await request(app).post('/site').send(siteData);

            expect(response.status).toBe(500);
            expect(response.body.error).toBe('Failed to create data');
        });
    });

    describe('PUT /site/:ticketNumber', () => {
        it('should update site location', async () => {
            connection.query.mockImplementation((sql, params, callback) => {
                callback(null, { affectedRows: 1 });
            });

            const response = await request(app).put('/site/123456').send({ location: 'New Location' });

            expect(response.status).toBe(200);
            expect(response.body.message).toBe('Site updated successfully');
        });

        it('should return 404 if site not found', async () => {
            connection.query.mockImplementation((sql, params, callback) => {
                callback(null, { affectedRows: 0 });
            });

            const response = await request(app).put('/site/123456').send({ location: 'New Location' });

            expect(response.status).toBe(404);
            expect(response.body.error).toBe('Site not found');
        });
    });

    describe('DELETE /site', () => {
        it('should delete site', async () => {
            connection.query.mockImplementation((sql, params, callback) => {
                callback(null, { affectedRows: 1 });
            });

            const response = await request(app).delete('/site').query({ ticketNumber: '123456' });

            expect(response.status).toBe(200);
            expect(response.body.message).toBe('Site deleted successfully');
        });

        it('should return 404 if site not found', async () => {
            connection.query.mockImplementation((sql, params, callback) => {
                callback(null, { affectedRows: 0 });
            });

            const response = await request(app).delete('/site').query({ ticketNumber: '123456' });

            expect(response.status).toBe(404);
            expect(response.body.error).toBe('Site not found');
        });
    });
});
